default_target: rebuild

build:
	@echo "Build repo"
	@sh build.sh

clear:
	@echo "Clear repo"
	@rm -rf own data/wakdata cdnwakfu building config_wakdata fetch.py

fetch:
	@sh fetch.sh

rebuild: clear build

rebuild_data: rebuild fetch