from cdnwakfu import Text

class Notify:

    notify = [
        Text({
            "fr":"toutes",
            "en":"all",
            "es":"all",
            "pt":"all"
        }),
        Text({
            "fr":"recettes-seulement",
            "en":"recipe-only",
            "es":"recipe-only",
            "pt":"recipe-only"
        }),
        Text({
            "fr":"aucune",
            "en":"none",
            "es":"none",
            "pt":"none"
        })
    ]

    default = 2