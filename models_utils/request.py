from cdnwakfu import Text

class Requests:

    craftRequest = Text({
        "en":"Craft Request",
        "fr":"Requête de Fabrication",
        "es":"Solicitud del arte",
        "pt":"Pedido de ofício"
    })

    componentRequest = Text({
        "en":"Component Request",
        "fr":"Requête de Composant(s)",
        "es":"Solicitud componente",
        "pt":"Pedido componente"
    })

    buyRequest = Text({
        "en":"Purchase Item Request",
        "fr":"Requête d'achat d'Objet(s)",
        "es":"Solicitud de compra de artículos",
        "pt":"Pedido de item de compra"
    })