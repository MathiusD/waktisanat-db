from cdnwakfu import Text
account_already_exist = Text({
    "fr":"Vous possedez déjà un compte",
    "en":"Account already exist",
    "es":"La cuenta ya existe",
    "pt":"A conta já existe"
})
account_created = Text({
    "fr":"Compte créé",
    "en":"Account Created",
    "es":"Cuenta creada",
    "pt":"Conta criada"
})
account_error_update = Text({
    "fr":"Erreur lors de la mise à jour de votre compte",
    "en":"Error in account updating",
    "es":"Error en actualización de la cuenta",
    "pt":"Erro em atualização de conta"
})
account_not_found_created = Text({
    "fr":"Compte inexistant, création..",
    "en":"Account doesn't exist, creating..",
    "es":"La cuenta no existe, creando..",
    "pt":"A conta não existe, criando.."
})
account_required = Text({
    "fr":"Vous devez avoir un compte",
    "en":"You must have a account",
    "es":"Debe tener una cuenta",
    "pt":"Deve ter uma conta"
})
account_update = Text({
    "fr":"Compte mis à jour",
    "en":"Account Updated",
    "es":"Cuenta actualizada",
    "pt":"Conta atualizada"
})
avatar_deleted = Text({
    "fr":"Avatar Supprimé",
    "en":"Avatar Deleted",
    "es":"Avatar suprimido",
    "pt":"Avatar eliminado"
})
avatar_register = Text({
    "fr":"Avatar Enregistré",
    "en":"Avatar Register",
    "es":"Registro del avatar",
    "pt":"Registro de avatar"
})
avatar_not_available = Text({
    "fr":"Un avatar avec le même nom est déjà enregistré avec ce nom sur ce compte",
    "en":"Avatar with this name is already register in this account",
    "es":"El avatar con este nombre es ya el registro en esta cuenta",
    "pt":"O avatar com este nome já é registro nesta conta"
})
bad_argument = Text({
    "fr":"Argument non valide, demande annulée",
    "en":"Argument invald, request cancel",
    "es":"Argumento invald, la solicitud anula",
    "pt":"Argumento invald, o pedido cancela"
})
chan_defined = Text({
    "fr":"Salon Défini",
    "en":"Channel defined",
    "es":"Canal definido",
    "pt":"Canal define-se"
})
notification_disable = Text({
    "fr":"Notifications Désactivées",
    "en":"Notifications Deactivated",
    "es":"Notificaciones desactivadas",
    "pt":"Notificações desativadas"
})
closed = Text({
    "fr":"Fermé à",
    "en":"Closed At",
    "es":"Cerrado en",
    "pt":"Fechado em"
})
cancel = Text({
    "fr":"Annuler",
    "en":"Cancel",
    "es":"Anular",
    "pt":"Cancelar"
})
canceled = Text({
    "fr":"Annulé",
    "en":"Canceled",
    "es":"Anulado",
    "pt":"Cancelado"
})
current = Text({
    "fr":"Actuel",
    "en":"Current",
    "es":"Actual",
    "pt":"Corrente"
})
confirm = Text({
    "fr":"Oui",
    "en":"Yes",
    "es":"Si",
    "pt":"Sim"
})
comments = Text({
    "fr":"Commentaires",
    "en":"Comments",
    "es":"Comentarios",
    "pt":"Comentários"
})
craft_fail = Text({
    "fr":"Vous n'avez pas le niveau requis pour tenter cette recette.",
    "en":"You have no level requested to attract this recipe.",
    "es":"No tiene nivel solicitado atraer esta receta.",
    "pt":"Não tem nível solicitado atrair esta receita."
})
desc = Text({
    "fr":"Description",
    "en":"Description",
    "es":"Descripción",
    "pt":"Descrição"
})
help_admin_note = Text({
    "fr":"Note : Commande utilisable uniquement au sein d'un serveur dont vous êtes \
administrateur.",
    "en":"Note: Usable Order only within a server administrator of which you are.",
    "es":"Nota: el Pedido Utilizable sólo dentro de un administrador del servidor \
del cual es.",
    "pt":"Nota: a Ordem Usável só dentro de um administrador de servidor do qual é."
})
def discord_desc(prefix:str):
    return {
        "global":Text({
            "fr":"Bot Wakfu pour l'Artisanat",
            "en":"Bot Wakfu for Handicrafts",
            "es":"Bot Wakfu para la artesanía",
            "pt":"Bot Wakfu de habilidade manual"
        }),
        "addAvatar":Text({
            "fr":"Pour enregistrer un Avatar sur votre compte\n\
Utilisation : `%sajouterAvatar <nomAvatar> <serveur> [Optionnel] <niveau> [Optionnel]`" % prefix,
            "en":"For Register Avatar in your account\n\
Use : `%saddAvatar <avatarName> <server> [Optional] <level> [Optional]`" % prefix,
            "es":"Para registrar Avatar en su cuenta\n\
Usar : `%sañadaAvatar <nombreDelAvatar> <servidor> [Opcional] <nivel> [Opcional]`" % prefix,
            "pt":"Para Avatar de Registro na sua conta\n\
Uso : `%sacrescenteAvatar <nomeDeAvatar> <servidor> [Opcional] <nível> [Opcional]`" % prefix
        }),
        "userAvatars":Text({
            "fr":"Pour afficher tout vos avatars ou les avatars de <nom_utilisateur>\n\
Utilisation : `%savatarUtilisateur <nomUtilisateur> [Optionnel]`" % prefix,
            "en":"For fetch your's avatars or avatars of <name>\n\
Use : `%suserAvatars <userName> [Optional]`" % prefix,
            "es":"Para buscar sus avatares o avatares de <nombre>\n\
Usar : `%savataresUsuario <nombreDeUsuario> [Opcional]`" % prefix,
            "pt":"Para o esforço o seu é avatares ou avatares do <nome>\n\
Uso : `%savataresUsuário <nomeDoUsuário> [Opcional]`" % prefix
        }),
        "searchAvatars":Text({
            "fr":"Pour afficher tout les avatars avec <nom>\n\
Utilisation : `%srechercherAvatar <nomAvatar>`" % prefix,
            "en":"For fetch avatars with <name>\n\
Use : `%ssearchAvatars <avatarName>`" % prefix,
            "es":"Para avatares del esfuerzo con <nombre>\n\
Usar : `%sbusqueAvatares <nombreDelAvatar>`" % prefix,
            "pt":"Para avatares de esforço com <nome>\n\
Uso : `%sprocureAvatares <nomeDeAvatar>`" % prefix
        }),
        "avatarCanCraft":Text({
            "fr":"Pour selectionner les avatars capable de fabriquer l'<objet>\n\
Utilisation : `%savatarPouvantFabriquer <nomObjet>`" % prefix,
            "en":"For show Avatars capable of making <item>\n\
Use : `%savatarCanCraft <itemName>`" % prefix,
            "es":"Para Avatares del espectáculo capaces de hacer <artículo>\n\
Usar : `%savatarCapazDeHacer <Nombre del artículo>`" % prefix,
            "pt":"Para Avatares de demonstração capazes de fazer <item>\n\
Uso : `%savatarCapazDeCriação <Nome de item>`" % prefix
        }),
        "favorite":Text({
            "fr":"Pour afficher, ajouter ou supprimer un favori\n\
Utilisation : `%sfavori <nomAvatar> [Optionnel]`" % prefix,
            "en":"For get, add or suppress favorite\n\
Use : `%sfavorite <avatarName> [Optional]`" % prefix,
            "es":"Para consiguen, añaden o suprimen al favorito\n\
Usar : `%sfavorito <nombreDelAvatar> [Opcional]`" % prefix,
            "pt":"Para adquirem, acrescentam ou suprimem o favorito\n\
Uso : `%sfavorito <nomeDeAvatar> [Opcional]`" % prefix
        }),
        "directory":Text({
            "fr":"Pour selectionner les avatars au sein de <serveur>\n\
Utilisation : `%sannuaire <serveur> [Optionnel] <métier> [Optionnel]`" % prefix,
            "en":"For select avatar in <server>\n\
Use : `%sdirectory <server> [Optional] <job> [Optional]`" % prefix,
            "es":"Para avatar escogido en <servidor>\n\
Usar : `%sdirectorio <servidor> [Opcional] <trabajo> [Opcional]`" % prefix,
            "pt":"Para avatar escolhido em <servidor>\n\
Uso : `%sdiretório <servidor> [Opcional] <trabalho> [Opcional]`" % prefix
        }),
        "removeAvatar":Text({
            "fr":"Pour supprimer un avatar avec <nom> au sein de vos avatars\n\
Utilisation : `%ssupprimerAvatar <nomAvatar>`" % prefix,
            "en":"For remove avatar with <name> in your's avatars\n\
Use : `%sremoveAvatar <avatarName>`" % prefix,
            "es":"Para quitan el avatar con el <nombre> en su es avatares \n\
Usar : `%squiteAvatar <nombreDelAvatar>`" % prefix,
            "pt":"Para retiram o avatar com o <nome> no seu é avatares \n\
Uso : `%sretireAvatar <nomeDeAvatar>`" % prefix
        }),
        "getJobLevel":Text({
            "fr":"Pour afficher le niveau d'un métier d'un de vos avatars\n\
Utilisation : `%sniveauMétier <nomAvatar> [Optionnel] <métier> [Optionnel]`" % prefix,
            "en":"For get JobLevel of Avatar in Job\n\
Use : `%sgetJobLevel <avatarName> [Optional] <job> [Optional]`" % prefix,
            "es":"Para consiguen JobLevel de Avatar en Trabajo\n\
Usar : `%sconsigaNivelTrabajo <nombreDelAvatar> [Opcional] <trabajo> [Opcional]`" % prefix,
            "pt":"Para adquirem JobLevel de Avatar em Emprego\n\
Uso : `%sadquiraNívelEmprego <nomeDeAvatar> [Opcional] <trabalho> [Opcional]`" % prefix
        }),
        "setJobLevel":Text({
            "fr":"Pour modifier le niveau d'un métier d'un de vos avatars\n\
Utilisation : `%smodifierNiveauMétier <niveau>`" % prefix,
            "en":"For set JobLevel of Avatar in Job\n\
Use : `%ssetJobLevel <level>`" % prefix,
            "es":"Para juego JobLevel de Avatar en Trabajo\n\
Usar : `%sjuegoNivelTrabajo <nombreDelAvatar> [Opcional] <trabajo> [Opcional]`" % prefix,
            "pt":"Para jogo JobLevel de Avatar em Emprego\n\
Uso : `%sjogoNívelEmprego <nomeDeAvatar> [Opcional] <trabalho> [Opcional]`" % prefix
        }),
        "level":Text({
            "fr":"Pour afficher ou modifier le niveau d'un de vos avatars\n\
Utilisation : `%sniveau <nomAvatar>[Optionnel] <niveau> [Optionnel]`" % prefix,
            "en":"For get or set Level of Avatar\n\
Use : `%slevel <avatarName> [Optional] <level> [Optional]`" % prefix,
            "es":"Para consiguen o ponen el Nivel del Avatar\n\
Usar : `%snivel <nombreDelAvatar> [Opcional] <nivel> [Opcional]`" % prefix,
            "pt":"Para adquirem ou estabelecem o Nível do Avatar\n\
Uso : `%snível <nomeDeAvatar> [Opcional] <nível> [Opcional]`" % prefix
        }),
        "pattern":Text({
            "fr":"Pour afficher, ajouter ou supprimer un Plan à un de vos avatars\n\
Utilisation : `%splans <nomAvatar> [Optionnel] <NomPlan> [Optionnel]`" % prefix,
            "en":"For get, add or suppres Pattern of Avatar\n\
Use : `%spattern <avatarName> [Optional] <patternName> [Optional]`" % prefix,
            "es":"Para consiguen, añaden o suprimen el Modelo del Avatar\n\
Usar : `%spatrón <nombreDelAvatar> [Opcional] <nombreDelModelo > [Opcional]`" % prefix,
            "pt":"Para adquirem, acrescentam ou suprimem o Modelo do Avatar\n\
Uso : `%spadrão <nomeDeAvatar> [Opcional] <nomeDeModelo> [Opcional]`" % prefix
        }),
        "missingPattern":Text({
            "fr":"Pour afficher les plans Manquants à l'un de vos avatars\n\
Utilisation : `%splansManquants <nomAvatar> [Optionnel]`" % prefix,
            "en":"For show Missing Pattern of Avatar\n\
Use : `%smissingPattern <avatarName> [Optional]`" % prefix,
            "es":"Para espectáculo Modelo Ausente de Avatar\n\
Usar : `%smodeloAusente <nombreDelAvatar> [Opcional]`" % prefix,
            "pt":"Para demonstração Modelo Ausente de Avatar\n\
Uso : `%smodeloAusente <nomeDeAvatar> [Opcional]`" % prefix
        }),
        "lang":Text({
            "fr":"Pour afficher ou modifier la langue par défaut au sein de votre serveur\n\
Utilisation : `%slangue <langue> [Optionnel]`\n%s" % (prefix, getattr(help_admin_note, "fr")),
            "en":"For get or set Default Lang in this Guild\n\
Use : `%slang <lang> [Optional]`\n%s" % (prefix, getattr(help_admin_note, "en")),
            "es":"Para consiguen o ponen a Default Lang en este Gremio\n\
Usar : `%slang <lang> [Opcional]`\n%s" % (prefix, getattr(help_admin_note, "es")),
            "pt":"Para adquirem ou estabelecem Default Lang nesta Corporação\n\
Uso : `%slang <lang> [Opcional]`\n%s" % (prefix, getattr(help_admin_note, "pt"))
        }),
        "notifyChannel":Text({
            "fr":"Pour afficher ou modifier le Salon de Notification du Bot\n\
Utilisation : `%ssalonNotifications <salon> [Optionnel]`\n%s" % (prefix, getattr(help_admin_note, "fr")),
            "en":"For get or set Notify Channel\n\
Use : `%snotifyChannel <channel> [Optional]`\n%s" % (prefix, getattr(help_admin_note, "en")),
            "es":"Para se ponen o se ponen Notifican Canal \n\
Usar : `%snotifiqueCanal <Canal> [Opcional]`\n%s" % (prefix, getattr(help_admin_note, "es")),
            "pt":"Para adquirem ou estabelecem Notificam Canal\n\
Uso : `%snotifiqueCanal <Canal> [Opcional]`\n%s" % (prefix, getattr(help_admin_note, "pt"))
        }),
        "prefix":Text({
            "fr":"Pour afficher ou modifier le préfixe du Bot\n\
Utilisation : `%spréfixe <préfixe> [Optionnel]`\n%s" % (prefix, getattr(help_admin_note, "fr")),
            "en":"For get or set Prefix\n\
Use : `%sprefix <prefix> [Optional]`\n%s" % (prefix, getattr(help_admin_note, "en")),
            "es":"Para consiguen o ponen el Prefijo\n\
Usar : `%sprefijo <prefijo> [Opcional]`\n%s" % (prefix, getattr(help_admin_note, "es")),
            "pt":"Para adquirem ou estabelecem o Prefixo\n\
Uso : `%sprefixo <prefixo> [Opcional]`\n%s" % (prefix, getattr(help_admin_note, "pt"))
        }),
        "showOwnIssues":Text({
            "fr":"Pour visualiser ses Tickets\n\
Utilisation : `%svoirSesTickets`" % prefix,
            "en":"For show own Issues\n\
Use : `%smostrarPropiasCuestiones`" % prefix,
            "es":"Ya que espectáculo posee Cuestiones\n\
Usar : `%smostrarPrópriasQuestões`" % prefix,
            "pt":"Já que demonstração possui Questões\n\
Uso : `%sacrescenteSugerem`" % prefix
        }),
        "addSuggest":Text({
            "fr":"Pour soumettre une Suggestion\n\
Utilisation : `%sajouterSuggestion`" % prefix,
            "en":"For adding suggest\n\
Use : `%saddSuggest`" % prefix,
            "es":"Ya que la adición sugiere\n\
Usar : `%sañadaSugieren`" % prefix,
            "pt":"Já que a soma sugere\n\
Uso : `%sacrescenteSugerem`" % prefix
        }),
        "addReport":Text({
            "fr":"Pour remonter une Erreur\n\
Utilisation : `%sreporterErreur`" % prefix,
            "en":"For adding report\n\
Use : `%saddReport`" % prefix,
            "es":"Para añadir informe\n\
Usar : `%sañadaInforme`" % prefix,
            "pt":"Para acrescentar o relatório\n\
Uso : `%sacrescenteRelatório`" % prefix
        }),
        "suggests":Text({
            "fr":"Pour afficher les Suggestions soumises au bot\n\
Utilisation : `%ssuggestions`" % prefix,
            "en":"For show suggests\n\
Use : `%ssuggests`" % prefix,
            "es":"Ya que el espectáculo sugiere\n\
Usar : `%ssugiere`" % prefix,
            "pt":"Já que a demonstração sugere\n\
Uso : `%ssugere`" % prefix
        }),
        "reports":Text({
            "fr":"Pour afficher les Erreurs remontées au bot\n\
Utilisation : `%serreurs`" % prefix,
            "en":"For show reports\n\
Use : `%sreports`" % prefix,
            "es":"Para informes del espectáculo\n\
Usar : `%sinformes`" % prefix,
            "pt":"Para relatórios de demonstração\n\
Uso : `%srelatórios`" % prefix
        }),
        "search":Text({
            "fr":"Pour chercher un Objet <nom> dans la <langue>[optionnel]\n\
Utilisation : `%srechercher <nom> <langue> [Optionnel]`" % prefix,
            "en":"For search Item with <name> in this <lang>[optional]\n\
Use : `%ssearch <name> <lang> [Optional]`" % prefix,
            "es":"Para Artículo de búsqueda con <nombre> en esto <lang> [opcional]\n\
Usar : `%sbuscar <nombre> <lang> [Opcional]`" % prefix,
            "pt":"Para Item de pesquisa com <nome> nisto <lang> [opcional]\n\
Uso : `%sbuscar <nome> <lang> [Opcional]`" % prefix
        }),
        "searchItemCraftWith":Text({
            "fr":"Pour chercher un Objet fabriqué avec un Objet <nom> dans la <langue>[optionnel]\n\
Utilisation : `%srechercherObjetFabriquéAPartirDe <nom> <langue> [Optionnel]`" % prefix,
            "en":"For search Item Craft with Item with <name> in this <lang>[optional]\n\
Use : `%ssearchItemCraftWith <name> <lang> [Optional]`" % prefix,
            "es":"Para Arte del Artículo de búsqueda con Artículo con <nombre> en esto <lang> [opcional]\n\
Usar : `%sbusqueArtedelArtículoCon <nombre> <lang> [Opcional]`" % prefix,
            "pt":"Para Ofício de Item de pesquisa com Item com <nome> nisto <lang> [opcional]\n\
Uso : `%sprocureOfíciodeItemcom <nome> <lang> [Opcional]`" % prefix
        }),
        "searchItemWithCriteria":Text({
            "fr":"Pour chercher un Objet avec des Critère(s)\n\
Utilisation : `%srechercherObjetAvecCritères`" % prefix,
            "en":"For search Item with Criteria(s)\n\
Use : `%ssearchItemWithCriteria`" % prefix,
            "es":"Para Artículo de búsqueda con Criterios (criterios)\n\
Usar : `%sbusqueArtículoConCriterios`" % prefix,
            "pt":"Para Item de pesquisa com Critérios (critérios)\n\
Uso : `%sprocureItemComCritérios`" % prefix
        }),
        "addRequest":Text({
            "fr":"Pour soumettre une requête pour la fabrication de l'objet <name>\n\
Utilisation : `%ssoumettreRequête <nom> <nomAvatar> [Optionnel]`" % prefix,
            "en":"For add request for Item with <name>\n\
Use : `%saddRequest <name> <avatarName> [Optional]`" % prefix,
            "es":"Para añaden la petición del Artículo con el <nombre>\n\
Usar : `%sañadaSolicitud <nombre> <nombreDelAvatar> [Opcional]`" % prefix,
            "pt":"Para acrescentam o pedido no Item com <nome>\n\
Uso : `%sacrescentePedido <nome> <nomeDeAvatar> [Opcional]`" % prefix
        }),
        "requestsAvatar":Text({
            "fr":"Pour afficher les requêtes soumises avec l'un de vos avatars\n\
Utilisation : `%srequêtesAvatar <nomAvatar> [Optionnel]" % prefix,
            "en":"For show request in your's avatars\n\
Use : `%srequestsAvatar <avatarName> [Optional]`" % prefix,
            "es":"Ya que la solicitud del espectáculo en su es avatares\n\
Usar : `%savatarSolicitudes <nombreDelAvatar> [Opcional]`" % prefix,
            "pt":"Já que o pedido de demonstração no seu é avatares\n\
Uso : `%savatarPedidos <nomeDeAvatar> [Opcional]`" % prefix
        }),
        "requestsAvailable":Text({
            "fr":"Pour afficher les requêtes qui peuvent être résolue par l'un de vos avatars\n\
Utilisation : `%srequêtesAvatar <nomAvatar> [Optionnel]" % prefix,
            "en":"For show request available in your's avatars\n\
Use : `%srequestsAvailable <avatarName> [Optional]`" % prefix,
            "es":"Ya que la solicitud del espectáculo disponible en su es avatares\n\
Usar : `%ssolicitudesDisponibles <nombreDelAvatar> [Opcional]`" % prefix,
            "pt":"Já que o pedido de demonstração disponível no seu é avatares\n\
Uso : `%spedidosDisponíveis <nomeDeAvatar> [Opcional]`" % prefix
        }),
        "removeAllRequest":Text({
            "fr":"Pour supprimer toutes vos requêtes lié à l'un de vos avatars\n\
Utilisation : `%ssupprimerToutesRequêtes <nomAvatar> [Optionnel]`" % prefix,
            "en":"For flush request in your avatar\n\
Use : `%sremoveAllRequest <avatarName> [Optional]`" % prefix,
            "es":"Ya que el rubor solicita en su avatar\n\
Usar : `%squiteTodaSolicitud <nombreDelAvatar> [Opcional]`" % prefix,
            "pt":"Já que o rubor solicita no seu avatar\n\
Uso : `%sretireTodoPedido <nomeDeAvatar> [Opcional]`" % prefix
        }),
        "register":Text({
            "fr":"Pour vous enregistrer\n\
Utilisation : `%senregistrer <nom> [Optionnel] <langue> [Optionnel] <niveauDeNotification> [Optionnel] <préfixe> [Optionnel]`" % prefix,
            "en":"For register an account\n\
Use : `%saddAvatar <name> [Optional] <lang> [Optional] <notifyLevel> [Optional] <prefix> [Optional]`" % prefix,
            "es":"Para registro una cuenta\n\
Usar : `%sregistro <nombre> [Opcional] <lang> [Opcional] <notifiqueNivel> [Opcional] <prefijo> [Opcional]`" % prefix,
            "pt":"Para registro uma conta\n\
Uso : `%sregistro <nome> [Opcional] <lang> [Opcional] <notifiqueNível> [Opcional] <prefixo> [Opcional]`" % prefix
        }),
        "prefixAccount":Text({
            "fr":"Pour afficher ou modifier le préfixe du Bot\n\
Utilisation : `%spréfixeCompte <préfixe> [Optionnel]`" % prefix,
            "en":"For get or set Prefix\n\
Use : `%sprefixAccount <prefix> [Optional]`" % prefix,
            "es":"Para consiguen o ponen el Prefijo\n\
Usar : `%scuentaPrefijo <prefijo> [Opcional]`" % prefix,
            "pt":"Para adquirem ou estabelecem o Prefixo\n\
Uso : `%scontaPrefixo <prefixo> [Opcional]`" % prefix
        }),
        "langAccount":Text({
            "fr":"Pour afficher ou modifier la langue liée à votre compte\n\
Utilisation : `%slangueCompte <langue> [Optionnel]`" % prefix,
            "en":"For get or set lang in your account\n\
Use : `%slangAccount <lang> [Optional]`" % prefix,
            "es":"Para consiguen o ponen lang en su cuenta\n\
Usar : `%scuentaLang <lang> [Opcional]`" % prefix,
            "pt":"Para adquirem ou estabelecem lang na sua conta\n\
Uso : `%scontaLang <lang> [Opcional]`" % prefix
        }),
        "nameAccount":Text({
            "fr":"Pour afficher ou modifier le nom de votre compte\n\
Utilisation : `%snomCompte <nom> [Optionnel]`" % prefix,
            "en":"For get or set name in your account\n\
Use : `%snameAccount <name> [Optional]`" % prefix,
            "es":"Para consiguen o definen el nombre en su cuenta\n\
Usar : `%snombreCuenta <nombre> [Opcional]`" % prefix,
            "pt":"Para adquirem ou definem o nome na sua conta\n\
Uso : `%snomeConta <nome> [Opcional]`" % prefix
        }),
        "notify":Text({
            "fr":"Pour afficher ou modifier le niveau de notification sur votre compte\n\
Utilisation : `%snotification <niveauDeNotification> [Optionnel]`" % prefix,
            "en":"For get or set notify level in your account\n\
Use : `%snotify <notifyLevel> [Optional]`" % prefix,
            "es":"Para se ponen o se ponen notifican el nivel en su cuenta\n\
Usar : `%sinformar <notifiqueNivel> [Opcional]`" % prefix,
            "pt":"Para adquirem ou estabelecem notificam o nível na sua conta\n\
Uso : `%snotificar <notifiqueNível> [Opcional]`" % prefix
        }),
        "help":Text({
            "fr":"Pour afficher cette commande ou afficher l'aide de <commande>\n\
Utilisation : `%shelp <nomCommande||nomCatégorie> [Optionnel]`" % prefix,
            "en":"For show this command or show help of <command>\n\
Use : `%shelp <commandName||categoryName> [Optional]`" % prefix,
            "es":"Para espectáculo esta orden o ayuda del espectáculo de <orden>\n\
Usar : `%sañadaAvatar <ordenNombre||categoríaNombre> [Opcional]`" % prefix,
            "pt":"Para demonstração esta ordem ou ajuda de demonstração de <ordem>\n\
Uso : `%sacrescenteAvatar <ordeneNome||categoriaNome> [Opcional]`" % prefix
        }),
        "supportGuild":Text({
            "fr":"Pour afficher le serveur discord de support\n\
Utilisation : `%ssupportServeur`" % prefix,
            "en":"For show Discord Support Guild\n\
Use : `%ssupportGuild`" % prefix,
            "es":"Para Gremio de Apoyo de la Discord del espectáculo\n\
Usar : `%sgremioApoyo`" % prefix,
            "pt":"Para Corporação de Suporte de Discord de demonstração\n\
Uso : `%scorporaçãoSuporte`" % prefix
        }),
        "getComponentsOfBuild":Text({
            "fr":"Pour afficher tout les composants nécessaire à la fabrication de votre Build.\n\
Note seul les codes provenant du builder de Slummp sont disponnible pour le moment.\n\
Utilisation : `%sgetComponentsOfBuild <code>`" % prefix,
            "en":"To display all the components needed to make your Build.\n\
Note only codes from the Slummp builder are currently available.\n\
Use : `%sgetComponentsOfBuild <code>`" % prefix,
            "es":"Para mostrar todos los componentes necesarios para hacer tu Build.\n\
Tenga en cuenta que actualmente sólo están disponibles los códigos del constructor de Slummp.\n\
Usar : `%sgetComponentsOfBuild <código>`" % prefix,
            "pt":"Expor todos os componentes tinha de fazer o seu Construir.\n\
A nota só codifica do construtor de Slummp estão atualmente disponíveis.\n\
Uso : `%sgetComponentsOfBuild <código>`" % prefix
        }),
        "compareComponentsOfBuilds":Text({
            "fr":"Pour comparer tout les composants nécessaire à la fabrication de votre Build à un autre.\n\
Note seul les codes provenant du builder de Slummp sont disponnible pour le moment.\n\
Utilisation : `%sgetComponentsOfBuild <premierCode> <secondCode>`" % prefix,
            "en":"To compare components very essentials with manufacture from your Build to other one.\n\
Note only codes from the Slummp builder are currently available.\n\
Use : `%sgetComponentsOfBuild <firstCode> <secondCode>`" % prefix,
            "es":"Comparar componentes muy elementos necesarios con fabricación de su Construir a otro.\n\
Tenga en cuenta que actualmente sólo están disponibles los códigos del constructor de Slummp.\n\
Usar : `%sgetComponentsOfBuild <primerCódigo> <segundoCódigo>`" % prefix,
            "pt":"Comparar componentes muito princípios básicos com a manufatura do seu Construir a outro.\n\
A nota só codifica do construtor de Slummp estão atualmente disponíveis.\n\
Uso : `%sgetComponentsOfBuild <primeiroCódigo> <segundoCódigo>`" % prefix
        }),
        "showItemInBuild":Text({
            "fr":"Pour afficher un objet particulier de votre Build.\n\
Note seul les codes provenant du builder de Slummp sont disponnible pour le moment.\n\
Utilisation : `%sshowItemInBuild <code>`" % prefix,
            "en":"To display a particular object in your Build.\n\
Note only codes from the Slummp builder are currently available.\n\
Use : `%sshowItemInBuild <code>`" % prefix,
            "es":"Para mostrar un objeto en particular en tu edificio.\n\
Tenga en cuenta que actualmente sólo están disponibles los códigos del constructor de Slummp.\n\
Usar : `%sshowItemInBuild <código>`" % prefix,
            "pt":"Expor um determinado objeto no seu Construir.\n\
A nota só codifica do construtor de Slummp estão atualmente disponíveis.\n\
Uso : `%sshowItemInBuild <código>`" % prefix
        }),
        "version":Text({
            "fr":"Pour afficher la version des données utilisées.\n\
Utilisation : `%sversion`" % prefix,
            "en":"To display the version of the data used.\n\
Use : `%sversion`" % prefix,
            "es":"Para mostrar la versión de los datos utilizados.\n\
Usar : `%sversion`" % prefix,
            "pt":"Expor a versão dos dados usados.\n\
Uso : `%sversion`" % prefix
        }),
        "no_cat":Text({
            "fr":"Sans Catégorie",
            "en":"No Categorie",
            "es":"Ningún Categorie",
            "pt":"Nenhum Categorie"
        }),
        "default":Text({
            "fr":"Aide non définie",
            "en":"Help not defined",
            "es":"Ayuda no definida",
            "pt":"Ajuda não definida"
        })
    }
execute_search = Text({
    "fr":"Executer la Recherche",
    "en":"Execute Search",
    "es":"Ejecutar la búsqueda",
    "pt":"Realize pesquisa"
})
error_occured = Text({
    "fr":"Une erreur est survenue",
    "en":"Error as occured",
    "es":"El error como ocurrió",
    "pt":"O erro como ocorreu"
})
fav_add = Text({
    "fr":"Favori Ajouté",
    "en":"Favorite add",
    "es":"Favorito añade",
    "pt":"Favorito acrescenta"
})
fav_suppress = Text({
    "fr":"Favori Supprimé",
    "en":"Favorite removed",
    "es":"Favorito quitado",
    "pt":"Favorito retira-se"
})
fav_suppress_request = Text({
    "fr":"Vous possedez déjà cet avatar en favori, le supprimer ? (%s pour confirmer, %s pour Annuler)" % (confirm.fr, cancel.fr),
    "en":"You already possedez this mishap in favourite, delete it? (%s to confirm, %s to cancel)" % (confirm.en, cancel.en),
    "es":"¿Usted ya possedez esta desgracia en favorito, bórralo? (%s para confirmar, %s para cancelar)" % (confirm.es, cancel.es),
    "pt":"Usted ya possedez esta desgracia en favorito, elimina-o? (%s para confirmar-se, %s para cancelar)" % (confirm.pt, cancel.pt)
})
have_not_recipe = Text({
    "fr":"Cet Avatar ne possède aucun plan.",
    "en":"This Avatar has no recipe.",
    "es":"Este Avatar no tiene ninguna receta.",
    "pt":"Este Avatar não tem receita."
})
have_all_recipe = Text({
    "fr":"Cet Avatar possède tout les plans.",
    "en":"Have All Recipe.",
    "es":"Tengan todas las recetas.",
    "pt":"Tenha toda a receita."
})
have_all_recipe_without = Text({
    "fr":"Cet Avatar possède tout les plans sauf :",
    "en":"Have All Recipe Without :",
    "es":"Tenga toda la receta sin :",
    "pt":"Tenha toda a receita sem :"
})
input_error = Text({
    "fr":"La commande a été mal saisie, affichage de l'aide associée",
    "en":"The command was mistyped, display of the associated help",
    "es":"El comando fue mal escrito, la visualización de la ayuda asociada",
    "pt":"A ordem foi mistyped, a exposição da ajuda associada"
})
issue = Text({
    "fr":"Ticket",
    "en":"Issue",
    "es":"Cuestión",
    "pt":"Questão"
})
issue_removed = Text({
    "fr":"Ticket supprimé",
    "en":"Issue removed",
    "es":"Cuestión quitada",
    "pt":"Questão retira-se"
})
issue_submit = Text({
    "fr":"Ticket Transmit",
    "en":"Issue Submit",
    "es":"Cuestión se rinde ",
    "pt":"Questão submete-se "
})
issue_updated = Text({
    "fr":"Ticket mis à jour",
    "en":"Issue Updated",
    "es":"Cuestión Actualizada",
    "pt":"Questão atualizada"
})
issue_not_submit = Text({
    "fr":"Ticket non Transmit",
    "en":"Issue not Submit",
    "es":"No se puede enviar el tema. . . ",
    "pt":"Questão não Se submete "
})
item_with_no_recipe = Text({
    "fr":"Cet Objet ne possède pas de recette",
    "en":"This Item haven't craft",
    "es":"Este Artículo no tiene arte",
    "pt":"Este Item não tem ofício"
})
job_lvl_incorrect = Text({
    "fr":"Le Niveau est incorrect (1 à 150)",
    "en":"Level isn't correct (1 to 150)",
    "es":"El nivel de vida no es correcto (1 a 150)",
    "pt":"Nível não é correto (1 para 150)"
})
job_not_exist = Text({
    "fr":"Ce métier n'existe pas",
    "en":"Job doesn't exist",
    "es":"El trabajo no existe",
    "pt":"O emprego não existe"
})
job_not_set = Text({
    "fr":"Métier non  Défini",
    "en":"Job isn't set",
    "es":"El trabajo no se pone",
    "pt":"O emprego não se estabelece"
})
job_retry = Text({
    "fr":"Reessayez avec ces métiers",
    "en":"Please retry with jobs",
    "es":"Por favor procese de nuevo con empleos",
    "pt":"Por favor reexperimente com empregos"
})
job_set = Text({
    "fr":"Métier Défini",
    "en":"Job set",
    "es":"Trabajo se puso",
    "pt":"Emprego estabelece-se"
})
lang_defined = Text({
    "fr":"Langue Définie",
    "en":"Lang defined",
    "es":"Trabajo se puso",
    "pt":"Lang define-se"
})
lang_suppress = Text({
    "fr":"Langue Supprimée",
    "en":"Lang suppress",
    "es":"Lang suprime",
    "pt":"Lang suprime"
})
lang_update_error = Text({
    "fr":"Erreur lors de la définition de la lang",
    "en":"Error in definition lang",
    "es":"Error en definición lang",
    "pt":"Erro em definição lang"
})
lang_used = Text({
    "fr":"Langue utilisée lors de la recherche : fr",
    "en":"Lang used for search : en",
    "es":"Lang utilizado para la búsqueda : es",
    "pt":"Lang usa-se para a pesquisa : pt"
})
link = Text({
    "fr":"Lien",
    "en":"Link",
    "es":"Relación",
    "pt":"Conexão"
})
lvl = Text({
    "fr":"Niveau",
    "en":"Level",
    "es":"Nivel",
    "pt":"Nível"
})
lvl_avatar_incorrect = Text({
    "fr":"Le Niveau est incorrect (1 à 215)",
    "en":"Level isn't correct (1 to 215)",
    "es":"El nivel de vida no es correcto (1 a 215)",
    "pt":"Nível não é correto (1 para 215)"
})
lvl_avatar_incorrect_creating = Text({
    "fr":"Le Niveau est incorrect (1 à 200), Niveau non enregistré",
    "en":"Level isn't correct (1 to 200), Level isn't register",
    "es":"El nivel de vida no es correcto (1 a 200), Nivel no está registrado.",
    "pt":"Nível não é correto (1 para 200), Nível não é registro "
})
lvl_avatar_set = Text({
    "fr":"Niveau Défini",
    "en":"Level set",
    "es":"Nivel se puso",
    "pt":"Nível estabelece-se"
})
lvl_avatar_not_set = Text({
    "fr":"Niveau non  Défini",
    "en":"Level isn't set",
    "es":"Nivel no se pone",
    "pt":"Nível não se estabelece"
})
missing_permissions = Text({
    "fr":"Vous n'avez pas les permissions requises pour executer cette commande.",
    "en":"You do not have the required permissions to execute this command.",
    "es":"No tienes los permisos necesarios para ejecutar este comando.",
    "pt":"Não tem as permissões necessárias de realizar esta ordem."
})
needing_by = Text({
    "fr":"Demandé par",
    "en":"Nedding by",
    "es":"Nedding por",
    "pt":"Nedding por"
})
no_account = Text({
    "fr":"Pas de compte enregistré",
    "en":"No account register for this user.",
    "es":"Ningún registro de la cuenta para este usuario.",
    "pt":"Nenhum registro de conta deste usuário."
})
no_avatar = Text({
    "fr":"Cet utilisateur ne possède pas d'avatar",
    "en":"This user don't have avatars",
    "es":"Este usuario no tiene avatares",
    "pt":"Este usuário não tem avatares"
})
no_avatar_with_given_name = Text({
    "fr":"Pas d'avatar avec ce nom",
    "en":"No avatar with given Name",
    "es":"Ningún avatar con nombre de pila",
    "pt":"Nenhum avatar com nome"
})
no_pattern_register = Text({
    "fr":"Vous n'avez pas de plans enregistré sur cet avatar",
    "en":"You don't have pattern register in this avatar",
    "es":"No tiene el registro del modelo en este avatar",
    "pt":"Não tem registro de modelo neste avatar"
})
no_pattern_missing = Text({
    "fr":"Vous n'avez pas de plans manquant sur cet avatar",
    "en":"You don't have pattern missing in this avatar",
    "es":"No te falta el patrón en este avatar...",
    "pt":"Não tem modelo que falha neste avatar"
})
def no_prefix_defined(prefix:str):
    return Text({
        "fr":"Aucun préfix défini (Utilisation de celui par défaut [%s])" % prefix,
        "en":"No prefix defined (Use the default one [%s])" % prefix,
        "es":"No se ha definido ningún prefijo (Utilice el predeterminado [%s])" % prefix,
        "pt":"Nenhum prefixo definiu (Use o default um [%s])" % prefix
    })
no_request = Text({
    "fr":"Aucune Demande enregistrée avec cet avatar",
    "en":"No request register in this avatar",
    "es":"Ningún registro de solicitud en este avatar",
    "pt":"Nenhum registro de pedido neste avatar"
})
no_request_available = Text({
    "fr":"Aucune Demande Disponible",
    "en":"No request available",
    "es":"Ninguna solicitud disponible",
    "pt":"Nenhum pedido disponível"
})
no_report_open = Text({
    "fr":"Aucun Report Ouvert",
    "en":"No reports Open",
    "es":"Ningunos informes Abiertos",
    "pt":"Nenhum relatório Aberto"
})
no_renseigned = Text({
    "fr":"Non renseigné",
    "en":"No renseigned",
    "es":"Not renseigned",
    "pt":"Nenhum renseigned"
})
no_suggest = Text({
    "fr":"Aucune Suggestion",
    "en":"No suggest",
    "es":"No sugiera",
    "pt":"Não sugira"
})
not_found = Text({
    "fr":"Non trouvé",
    "en":"Not Found",
    "es":"No encontrado",
    "pt":"Não encontrado"
})
no_favs = Text({
    "fr":"Aucun favori enregistré",
    "en":"No favorites registered",
    "es":"No hay favoritos registrados",
    "pt":"Nenhum favorito se registrou"
})
no_xp_earned = Text({
    "fr":"Cette recette ne vous rapportera pas d'expérience.",
    "en":"This recipe won't bring you any experience.",
    "es":"Esta receta no te traerá ninguna experiencia.",
    "pt":"Esta receita não lhe trará nenhuma experiência."
})
pattern_add = Text({
    "fr":"Plan Ajouté",
    "en":"Pattern add",
    "es":"Modelo añade",
    "pt":"Modelo acrescenta"
})
pattern_suppress = Text({
    "fr":"Plan Supprimé",
    "en":"Pattern removed",
    "es":"Modelo quitado",
    "pt":"Modelo retira-se"
})
pattern_suppress_request = Text({
    "fr":"Vous possedez déjà ce plan, le supprimer ? (%s pour confirmer, %s pour Annuler)" % (confirm.fr, cancel.fr),
    "en":"Do you already have this pattern, delete it? (%s to confirm, %s to cancel)" % (confirm.en, cancel.en),
    "es":"¿Ya tienes este patrón, bórralo? (%s para confirmar, %s para cancelar)" % (confirm.es, cancel.es),
    "pt":"Já tem este modelo, elimina-o? (%s para confirmar-se, %s para cancelar)" % (confirm.pt, cancel.pt)
})
prefix_defined = Text({
    "fr":"Préfixe Défini",
    "en":"Prefix defined",
    "es":"Prefijo definido",
    "pt":"Prefixo define-se"
})
prefix_suppress = Text({
    "fr":"Préfixe Supprimé",
    "en":"Prefix suppress",
    "es":"Prefijo suprime",
    "pt":"Prefixo suprime"
})
processing = Text({
    "fr":"Veuillez patientez",
    "en":"Wait Bot processing",
    "es":"Espere procesamiento de la Larva",
    "pt":"Espere Bot que processa"
})
recipe = Text({
    "fr":"Recette",
    "en":"Recipe",
    "es":"Receta",
    "pt":"Receita"
})
recipes = Text({
    "fr":"Recettes",
    "en":"Recipes",
    "es":"Recetas",
    "pt":"Receitas"
})
request = Text({
    "fr":"Demande",
    "en":"Request",
    "es":"Solicitud",
    "pt":"Pedido"
})
request_already_submit = Text({
    "fr":"Vous avez déjà soumit cette demande avec cet avatar",
    "en":"You have alredy submit this request in this avatar",
    "es":"Tiene ya presentan esta solicitud en este avatar",
    "pt":"Já tem submetem este pedido neste avatar"
})
request_submit = Text({
    "fr":"Demande Enregistrée",
    "en":"Request Submit",
    "es":"Solicitud se rinde",
    "pt":"Pedido submete-se"
})
request_empty = Text({
    "fr":"Demande Vide",
    "en":"Request Empty",
    "es":"Solicitud vacía",
    "pt":"Pedido vazio"
})
request_unsbmit = Text({
    "fr":"Demande annulée",
    "en":"Request Unsbmit",
    "es":"Solicitud Unsbmit",
    "pt":"Pedido Unsbmit"
})
requests_unsbmit = Text({
    "fr":"Vos demandes on été annulées",
    "en":"Requests are unsbmit",
    "es":"Las solicitudes son unsbmit",
    "pt":"Os pedidos são unsbmit"
})
request_updated = Text({
    "fr":"Demande mise à jour",
    "en":"Request Updated",
    "es":"Solicitud actualizada",
    "pt":"Pedido atualizado"
})
request_can_be_resolve_by = Text({
    "fr":"La requête peut être résolue par",
    "en":"Request can be resolve by",
    "es":"La solicitud puede ser la resolución por",
    "pt":"O pedido pode ser resolução por"
})
select = Text({
    "fr":"Choisissez l'un d'entre eux:",
    "en":"Please select one of this:",
    "es":"Por favor seleccione uno de esto:",
    "pt":"Por favor selecione um disto: "
})
send = Text({
    "fr":"Envoyé",
    "en":"Send",
    "es":"Mendar",
    "pt":"Enviar"
})
server = Text({
    "fr":"Serveur",
    "en":"Server",
    "es":"Servidor",
    "pt":"Servidor"
})
server_not_exist = Text({
    "fr":"Le serveur n'existe pas, choisit en un",
    "en":"Server doesn't exist please chose one",
    "es":"El servidor no existe por favor eligió el que",
    "pt":"O servidor não existe por favor escolheu aquele"
})
state = Text({
    "fr":"Etat",
    "en":"State",
    "es":"Estado",
    "pt":"Estado"
})
submit = Text({
    "fr":"Créé à",
    "en":"Submit At",
    "es":"Ríndase en",
    "pt":"Submit At"
})
user = Text({
    "fr":"Utilisateur",
    "en":"User",
    "es":"Usuario",
    "pt":"Usuário"
})
user_not_found = Text({
    "fr":"Utilisateur non Trouvé",
    "en":"User not Found",
    "es":"Usuario no Encontrado",
    "pt":"Usuário não Encontrado"
})
quickstart = Text({
    "fr":"Enchanté cher Douzien,\n\
Voici comment configurer rapidement WakTisanat, \
que je vais détailler en 2 points. En premier lieu \
pour l'utilisateur dit `lambda`, puis pour un serveur.\n\
__Pour un utilisateur `lambda` :__\n\
\t Afin de faire en sorte que votre expérience vous soit \
la plus agréable possible, je vous conseille de vous \
enregistrer via la commande `~register`.\n\
\t Si vous avez un compte, je vous conseille de choisir \
votre langue par défaut à travers la commande `~langAccount \
fr\en\es\pt`.\n\
\t Si vous avez l'habitude d'utiliser votre propre préfixe \
ou tout simplement que vous trouvez `~` insupportable, vous \
pouvez changer celui-ci à travers la commande `~prefix \
MonPrefixIncroyable`.\n\
\t__Besoin de fabriquer ?__\n\
\t\t Vous pouvez rechercher un objet à travers diverses \
commandes de recherches (`~help Objet` pour avoir plus de \
détails sur celles-ci). Ici, on va prendre la commande de base \
pour exemple `~search La Bonne Pioche` pour chercher la \
fiche de la Bonne Pioche. Sur celle-ci vous pouvez afficher \
ou masquer le craft via l'émote 🛠️, vous pouvez afficher ou \
masquer les statistiques via l'émote 🗡️, soumettre une \
demande de craft via l'émote 🔎, voir les artisans pouvant \
répondre à votre demande grâce à l'émote 📒 ou voir tous \
les composants et sous composants du craft avec l'émote 📃.\n\
\t\t Cependant, pour effectuer une requête, il vous faut \
enregistrer un avatar afin de permettre au bot de savoir \
sur quel serveur vous souhaiter effectuer le craft. Il vous \
suffit de faire `~addAvatar \"NomDeVotreAvatar\" VotreServeur`.\n\
\t\t Vous cherchez tous les artisans d'un serveur, c'est \
possible à l'aide de la commande `~directory Serveur`.\n\
\t__Vous êtes artisan ?__\n\
\t\t Au même titre que les personnes souhaitant fabriquer \
des objets, il vous faut enregistrer un avatar si vous souhaitez \
pouvoir être visible pour d'autres joueurs ayant potentiellement \
besoin de vos services. Une fois fait, il serait de bon ton \
d'enregistrer vos niveaux de métier à travers la commande \
`~setJobLvl 145` puis de selectionner le métier à définir dans \
la liste que le bot vous présentera.\n\
\t\t Vous n'êtes pas n'importe quel artisan et vous possédez des \
plans ? Pas de souci, vous pouvez également les enregistrer à \
travers la commande `~pattern \"NomDeVotreAvatar\" \"Le Seum\"` \
(ici vous enregistrez le fait que vous possédez le plan pour craft \
`Le Seum`).\n\
\t\t Vous souhaitez voir vos avatars afin de montrer vos plans et \
métiers à vos guildeux ? Pas de souci, il vous suffit de faire \
`~userAvatars` et le tour est joué.\n\
\t\t Vous souhaitez que le bot vous prévienne en cas de demandes \
auxquelles vous pouvez répondre ? Vous pouvez modifier vos \
paramètres de notification afin de recevoir soit toutes les \
demandes (`~notify toutes`), les demandes qui nécessitent un plan \
spécifique (`~notify recettes-seulement`) ou aucune (`~notify \
aucune`) (c'est le niveau de notification par défaut). Peu importe \
votre niveau de notification, vous pourrez voir à tout moment à \
quelles demandes vous pouvez répondre à travers \
`~requestsAvailable`.\n\
\t__Vous souhaitez préparer votre nouvel équipement ?__\n\
\t\t Vous pouvez voir le coût en composants de votre équipement à \
travers la commande `~getComponentsOfBuild`.\n\
\t\t Vous pouvez a contrario comparer la différence de composants \
entre deux équipements à l'aide de la commande \
`~compareComponentsOfBuilds`.\n\
__Pour un serveur :__\n\
\t Attention, les commandes suivantes nécessitent que vous aillez \
les permissions Administrateur sur le serveur (les mêmes permissions \
qui vous ont permis d'inviter Waktisanat à vrai dire).\n\
\t Vous souhaitez que le bot parle dans une langue définie sur votre serveur, il \
vous suffit de faire `~lang fr/en/es/pt`.\n\
\t Vous voulez définir un préfixe pour votre serveur car `~` est toujours aussi \
peu pratique. Vous pouvez le faire à travers `~prefix \"NouveauPréfix\"`.\n\
\t Avoir les notifications du bot est également possible. Pour cela il vous \
suffit de saisir la commande `~notifyChannel #VotreSalonDeNotif` et ainsi le \
bot vous préviendra des modifications apportées ou s'il redémarre car les \
données d'Ankama ont été mises à jour.\n\
__Questions, remarques, problèmes ?__\n\
\tVous avez un bug à signaler ? Dans ce cas, vous pouvez le remonter à \
l'aide de la commande `~addReport`.\n\
\tUne suggestion ? Vous pouvez la transmettre à travers `~addSuggest`.\n\
\tUne question sur le fonctionnement du bot ? Nous serons ravis de répondre \
à vos question sur le serveur de support (`~supportGuild` vous donnera le lien).\
",
    "en":"Delighted dear Douzien,\n\
Here is how to quickly configure WakTisanat, which I will detail in 2 points. \
First for the user called `lambda`, then for a server.\n\
__For a `lambda` user :__ \n\
\t In order to make your experience as favorable as possible, I advise you to \
register via the `~register` command.\n\
\t If you have an account I advise you to choose your default language through \
the `~langAccount fr\en\es\pt` command.\n\
\t If you are used to using your own prefix or simply find `~`unportable you \
can change it with the `~prefix MyUnbelievablePrefix` command.\n\
\t__Need to Craft ?__\n\
\t\t You can search for an object through various search commands \
(`~help Object` for more details on these). Here we'll take the basic \
command for example `~search the Good Pickaxe's` for the Good \
Pickaxe's file. On it you can show or hide craft of item with emote 🛠️, \
you can show or hide stats of item with emote 🗡️, \
you can submit a craft request through the emote 🔎, \
see the craftsmen who can answer your request through the emote 📒 or see \
all the components and sub-components of the craft with the emote 📃.\n\
\t\t However, to make a request, you need to register an avatar so that \
the bot knows on which server you want to make the craft. Just do \
`~addAvatar \"NameOfYourAvatar\" \"YourServer\"`.\n\
\t\t If you are looking for all the craftsmen of a server, you can do \
it with the command `~directory Server`.\n\
\t__You're a craftsman?__\n\
\t\t Just like people who want to make items, you need to register anavatar \
if you want to be visible to other players who may need your services. Once \
done, it would be a good idea to register your job levels with the command \
`~setJobLvl 145` and then select the job to define from the list the bot \
will present you with.\n\
\t\t You are not just any craftsman and you have blueprints, no worries you \
can also register them through the command `~pattern \"NameOfYourAvatar\" \
\"The Seum\"` (Here you register the fact that you own the blueprint for \
crafting `The Seum`).\n\
\t\t If you want to see your avatars to show your blueprints and trades to \
a guild no worries, just do `~userAvatars` and that's it.\n\
\t\t Do you want the bot to warn you in case of a request you can answer? \
You can change your notification settings to receive either all requests \
(`~notify all`), requests that require a specific plan (`~notify recipes-only`) \
or none (`~notify none`) [This is the default notification level]. No matter \
what level of notification you have, you will be able to see at any time \
which requests you can respond to through `~requestsAvailable`.\n\
\t__Do you want to prepare your new equipment ?__\n\
\t\tYou can see the component cost of your equipment through the \
`~getComponentsOfBuild` command.\n\
\t\tYou can compare the difference in components between two units \
with the `~compareComponentsOfBuilds` command.\n\
__For a Server :__\n\
\t Be careful the following commands require that you have Administrator \
permissions on the server (the same permissions that allowed you to invite \
Waktisanat actually).\n\
\t You want the bot to speak in a language defined on your server, just do \
`~lang fr/en/es/pt`. 	You want to set a prefix for your server because `~` \
is always inconvenient. You can do it through `~prefix \"New Prefix\"`.\n\
\t It is also possible to get notifications from the bot for this purpose. \
Simply enter the command `~notifyChannel #YourNotificationLounge` and the \
bot will notify you of any changes made or if it restarts because Ankama's \
data has been updated.\n\
__Questions, Comments, Problems?__\n\
\tYou have a bug to report, in this case you can report it with the command \
`~addReport`\n\
\tA suggestion, you can transmit it through `~addSuggest`\n\
\tA question about how the bot works? We will be happy to answer your questions \
on the support server (`~supportGuild` will give you the link).\
",
    "es":"TODO",
    "pt":"TODO"
})
quickstart_title = Text({
    "fr":"Démarrage Rapide",
    "en":"Quick Start",
    "es":"Comienzo Rápido",
    "pt":"Partida Rápida"
})
repos = {
    "waktisanat":Text({
        "fr":"Sur le bot de manière générale",
        "en":"On the bot in general",
        "es":"En el bot en general",
        "pt":"Em Bot em geral"
    }),
    "waktisanat-web":Text({
        "fr":"Sur le site web de manière générale",
        "en":"On the website in general",
        "es":"En el sitio web en general",
        "pt":"No site web em geral"
    }),
    "wakdata":Text({
        "fr":"Sur les données d'Ankama ou des builders",
        "en":"On the data of Ankama or builders",
        "es":"En los datos de Ankama o constructores",
        "pt":"Nos dados de Ankama ou construtores"
    }),
    "waktisanat-db":Text({
        "fr":"Sur les données enregistrées par WakTisanat",
        "en":"On the data recorded by WakTisanat",
        "es":"En los datos registrados por WakTisanat",
        "pt":"Nos dados registrados por WakTisanat"
    }),
    "*":Text({
        "fr":"Sur tout",
        "en":"On all",
        "es":"En todos",
        "pt":"Em todos"
    })
}
about = {
    "What":Text({
        "fr":"WakTisanat est un bot discord et un site web ayant pour but de \
consulter les objets du jeu afin de préparer son farm en amont.\n\
C'est également un annuaire recensant les artisans enregistrés et permettant \
de leur faire des requêtes pour simplifier son farm en jeu.",
        "en":"WakTisanat is a discord bot and a website whose goal is to consult \
the items in the game in order to prepare your farm beforehand.\n\
It is also a directory listing registered artisans and allowing you to make \
requests to simplify your farm in game.",
        "es":"WakTisanat es un robot de la discordia y un sitio web cuyo objetivo \
es consultar los elementos del juego para preparar su granja de antemano.\n\
También es un directorio que enumera los artesanos registrados y que le permite \
hacer solicitudes para simplificar su granja en la caza.",
        "pt":"WakTisanat é uma discórdia Bot e um site web cuja meta é consultar \
os itens no jogo para preparar a sua fazenda anteriormente.\n\
Também é um diretório que enumera artesãos registrados e permite-lhe fazer \
pedidos de simplificar a sua fazenda no jogo."
    }),
    "Why":Text({
        "fr":"Ce projet est un projet fanmade que j'ai initié lors du confinement \
2020 afin de tuer le temps et de permettre à ma guilde d'avoir une encylopédie sur \
discord.",
        "en":"This project is a fanmade project that I initiated during the \
confinement 2020 in order to kill time and allow my guild to have an encyclopedia \
on discord.",
        "es":"Este proyecto es un proyecto de fanmade que inicié durante el \
confinamiento 2020 a fin de matar el rato y permitir que mi gremio tenga una \
enciclopedia en la discordia.",
        "pt":"Este projeto é um projeto de fanmade que iniciei durante o \
confinamento 2020 para matar o tempo e permitir à minha corporação ter uma \
enciclopédia na discórdia."
    }),
    "Who":Text({
        "fr":"Sur une idée tordue de Mathius.\n\
Je remercie tout particulièrement :\n\
- Ankama pour la mise à disposition des données que vous pouvez consulter.\n\
- ikikay pour son aide sur l'aspect actuel du site web.\n\
- Enio pour son aide auprès de tout les fanDevs.\n\
- Isp, Slummp et Keyde pour leur réactivité lors de mes demandes afin de \
mettre en relation WakTisanat et les divers Builders existants.\n\
- Batleforc pour l'aide apporté lors du changement d'hébergement des services \
liés à WakTisanat.\n\
- Tarek Drevan, Omi, Hiro. et la Guilde Bric à Brac pour les lots de \
tests qu'ils ont effectués tout du long de l'évolution de WakTisanat.\n\
- Charlie pour les relectures qu'il a pû effectuer sur le site et le bot.\n\
- Ja'shurheso'do pour les relectures de certaines traductions anglaises (Sur \
l'interface uniquement puisque les données d'Ankama sont déjà traduite).\n\
- Neige pour les retours qu'il a pu faire afin de corriger les données impactant \
les plans notamment.\n\
- Le discord Wakfu[FR] et top.gg pour le recensement du bot.\n",
        "en":"On a twisted idea of Mathius'.\n\
Special thanks to:\n\
- Ankama for making the data available for you to consult.\n\
- ikikay for his help on the current look of the website.\n\
- Enio for his help with all the fanDevs.\n\
- Isp, Slummp and Keyde for their reactivity during my requests to put \
WakTisanat in contact with the various existing Builders.\n\
- Batleforc for its help in changing the hosting of WakTisanat related services.\n\
- Tarek Drevan, Omi, Hiro. and the Guild Bric à Brac for the batches of tests \
they have carried out throughout the evolution of WakTisanat.\n\
- Charlie for any proofreading he may have done at the site and the bot.\n\
- Ja'shurheso'do for proofreading some English translations (On the interface \
only since Ankama's data is already translated).\n\
- Neige for the returns he was able to make in order to correct the data impacting \
the plans in particular.\n\
- The Wakfu discord [FR] and top.gg for bot recency.\n",
        "es":"En una idea enroscada de Mathius'.\n\
Agradecimiento especial to:\n\
- Ankama para hacer los datos disponibles para usted para consultar \n\
- ikikay por su ayuda en el aspecto actual del sitio web.\n\
- Enio para su ayuda con todo el fanDevs.\n\
- Isp, Slummp y Keyde para su reactividad durante mis solicitudes de poner \
WakTisanat en contacto con varios Constructores existentes\n\
- Batleforc para su ayuda en cambio de la recepción de servicios relacionados \
con WakTisanat\n\
- Tarek Drevan, Omi, Hiro. y el Guild Bric à Brac para las hornadas de pruebas \
han realizado durante la evolución de WakTisanat. \n\
- Charlie para cualquier corrección de pruebas puede haber hecho en el sitio y \
la larva \n\
- Ja'shurheso'do para corregir las pruebas de algunas traducciones inglesas \
(En el interfaz sólo ya que los datos de Ankama se traducen ya). \n\
- Neige para las vueltas que era capaz de hacer a fin de corregir los datos \
hacer impacto los proyectos en detalle \n\
- La discordia Wakfu[FR] y top. gg para larva recency. \n",
        "pt":"Em uma ideia torcida de Mathius'.\n\
Obrigado especial to:\n\
- Ankama para fazer os dados disponíveis para você para consultar-se \n\
- ikikay para sua ajuda na aparência atual do site .\n\
- Enio da sua ajuda com todo o fanDevs.\n\
- Isp, Slummp e Keyde da sua reatividade durante os meus pedidos de pôr \
WakTisanat em contato com vários Construtores existentes.\n\
- Batleforc da sua ajuda em modificação da hospedagem de serviços \
relacionados a WakTisanat.\n\
- Tarek Drevan, Omi, Hiro. e o Guild Bric à Brac dos lotes de testes \
executaram durante a evolução de WakTisanat.\n\
- Charlie de qualquer revisão de provas pode ter feito no sítio e o bot.\n\
- Ja'shurheso'do para revisar algumas traduções inglesas (Na interface \
só desde que os dados de Ankama já se traduzem).\n\
- Neige dos regressos que foi capaz de fazer para corrigir o impacto de dados \
os planos em pormenor.\n\
- A discórdia Wakfu [FRANCO] e top. gg de novidade de Bot.\n"
    })
}
def unknown_error(prefix:str):
    return Text({
        "fr":"Une erreur est survenue. Nous sommes désolé pour cela. Cependant vous pouvez nous aider en la remontant à l'aide de la commande `%sreporterErreur` (Ou voir si cela a déjà été fait avec la commande `%serreurs`)." % (prefix, prefix),
        "en":"An error has occurred. We're sorry about that. However, you can help us by reporting it using the `%saddReport` command (Or see if this has already been done with the `%sreports` command). " % (prefix, prefix),
        "es":"Se ha producido un error. Lo sentimos. Sin embargo, puedes ayudarnos informándonos con el comando `%sañadaInforme` (o ver si ya se ha hecho con el comando `%sinformes`). " % (prefix, prefix),
        "pt":"Um erro ocorreu. Sentimos sobre isto. Contudo, pode ajudar-nos informando-o usando a ordem de `%sacrescenteRelatório` (Ou ver se isto já se fez com a ordem de `%srelatórios`). "% (prefix, prefix)
    })
no_price = Text({
    "fr":"Aucun prix renseigné par la communauté. (Vous pouvez le renseignez via l'émote 🏷️)",
    "en":"No prices given by the community. (You can give information to him via the reaction 🏷️)",
    "es":"No hay precios dados por la comunidad. (Le puede dar la información vía la reacción ️🏷️)",
    "pt":"Nenhum preço dado pela comunidade. (Pode dar-lhe a informação via a reação ️️🏷️)"
})
average_price = Text({
    "fr":"Prix Moyen",
    "en":"Medium Price",
    "es":"Precio medio",
    "pt":"Preço médio"
})
min_price = Text({
    "fr":"Prix Minimum",
    "en":"Price Minimum",
    "es":"Mínimo de precios",
    "pt":"Mínimo de preços"
})
max_price = Text({
    "fr":"Prix Maximum",
    "en":"Price Maximum",
    "es":"Máximo de precios",
    "pt":"Máximo de preços"
})
price_submit = Text({
    "fr":"Prix Enregistré",
    "en":"Price Submit",
    "es":"Máximo se rinde",
    "pt":"Máximo submete-se"
})
directoryText = Text({
    "fr":"Annuaire",
    "en":"Directory"
})
logout = Text({
    "fr":"Deconnexion",
    "en":"Logout"
})
login = Text({
    "fr":"Connexion",
    "en":"Login"
})
langs = Text({
    "fr":"Langues",
    "en":"Langs"
})
short_desc = Text({
    "fr":"Bot et Fansite d'artisanat Wakfu",
    "en":"Bot and Website for crafting in Wakfu"
})
profile = Text({
    "fr":"Profil",
    "en":"Profile"
})
missing_react_partial_item = Text({
    "fr":"Le bot ne possède pas les permissions pour joindre des émotes, ainsi vous ne pouvez pas profiter pleinement des fonctions du bot.",
    "en":"The bot doesn't have permissions to join emotes, so you can't take full advantage of the bot's features."
})
account_suppress_request = Text({
    "fr":"Vous vous apprêter à supprimer votre compte, si cela est voulu tapez `%s`. (%s pour Annuler)" % (confirm.fr, cancel.fr),
    "en":"You are about to delete your account, if desired type '%s'. (%s to Cancel)" % (confirm.en, cancel.en)
})
account_deleted = Text({
    "fr":"Votre compte a été supprimé avec succès.",
    "en":"Your account has been successfully deleted."
})