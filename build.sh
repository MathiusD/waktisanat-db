git clone https://gitlab.com/MathiusD/owninstall;
cp owninstall/install.py own_install.py;
rm -rf owninstall;
python3 build_here.py;
# Wakdata Import
echo "Fetch WakData";
git clone https://gitlab.com/MathiusD/wakdata;
FILE=./config/settings_wakdata.py
if [ -f "$FILE" ]; then
    cp $FILE ./wakdata/config_wakdata/settings.py;
fi
echo "Update Repo Structuration for Fetch Data";
cp -r wakdata/config_wakdata ./config_wakdata;
cp -r wakdata/cdnwakfu ./cdnwakfu;
cp -r wakdata/building ./building;
cp wakdata/fetch.py ./fetch.py;
cp wakdata/build.py ./build.py;
cp wakdata/build.sh ./wakdata.sh;
cp wakdata/requirements_wakdata.txt ./requirements_wakdata.txt;
sh wakdata.sh;
rm requirements_wakdata.txt;
echo "Clear WakData unused";
rm -rf ./wakdata;
rm ./wakdata.sh ./build.py;
rm own_install.py;