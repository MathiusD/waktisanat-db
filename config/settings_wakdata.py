URL_API = "https://wakfu.cdn.ankama.com/gamedata/"
URL_VERSION = "config.json"
NAME_JSON = [
    'actions',
    'collectibleResources',
    'equipmentItemTypes',
    'harvestLoots',
    'itemProperties',
    'items',
    'jobsItems',
    'recipeCategories',
    'recipeIngredients',
    'recipeResults',
    'recipes',
    'resourceTypes',
    'resources',
    'states',
    'recipePattern'
]
PATH_DATA = "data/wakdata"
LOCATION_REPO = "."