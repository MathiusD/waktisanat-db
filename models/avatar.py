from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
from models import User
from cdnwakfu import RecipeCategorie, RecipePattern, ServerElement, Server, Recipe
from data.wakdata import latest
import sqlite3

class Avatar:

    def __init__(self, db:sqlite3.Connection, users:User):
        self.db = db
        self.users = users
        fields = [
            Id,
            TextWithoutCaseSensitive("name", True),
            Integer("server", True),
            Integer("lvl"),
            Integer("id_user", True)
        ]
        foreign_key = [ForeignKey(fields[4], FieldRef(Id, self.users.model.name))]
        self.model = Model("Avatar", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key, db=db)
        fields_job = [
            Id,
            Integer("id_job", True),
            Integer("id_avatar", True),
            Integer("lvl", True)
        ]
        foreign_key_job = [ForeignKey(fields_job[2], FieldRef(Id, self.model.name))]
        self.model_job = Model("AvatarJob", fields=fields_job, primary_key=Default_PrimaryKey, foreign_key=foreign_key_job,db=db)
        fields_pattern = [
            Id,
            Integer("id_avatar", True),
            Integer("id_pattern", True)
        ]
        foreign_key_pattern = [ForeignKey(fields_pattern[1], FieldRef(Id, self.model.name))]
        self.model_pattern = Model("AvatarPattern", fields=fields_pattern, primary_key=Default_PrimaryKey, foreign_key=foreign_key_pattern,db=db)
        fields_favs = [
            Id,
            Integer("favOwner", True),
            Integer("favTarget", True)
        ]
        foreign_key_favs = [ForeignKey(fields_favs[1], FieldRef(Id, self.users.model.name)), ForeignKey(fields_favs[2], FieldRef(Id, self.model.name))]
        self.model_fav = Model("AvatarFav", fields=fields_favs, primary_key=Default_PrimaryKey, foreign_key=foreign_key_favs,db=db)

    def addAvatar(self, user:int, name:str, server:int, lvl:int = 0):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            for avatar in self.model.fetchByField(data.id, self.model.fields[4], self.db):            
                if name == avatar.name:
                    return None
            data = ModelData(self.model.fields, [None, name, server, lvl, data.id])
            return self._prepareAvatar(self.model.insertOne(data, self.db))
        return None

    def fetchAll(self):
        return self._prepareAvatars(self.model.fetchAll(self.db))

    def fetchAvatarsByDiscordId(self, user:int):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            return self.fetchAvatarsByUserId(data.id)
        return None

    def fetchAvatarsByUserId(self, id:int):
        return self._prepareAvatars(self.model.fetchByField(id, self.model.fields[4], self.db))

    def fetchAvatarsById(self, id:int):
        return self._prepareAvatar(self.model.fetchOneByField(id, self.model.fields[0], self.db))

    def renameAvatar(self, id:int, newName:str):
        avatar = self.fetchAvatarsById(id)
        if avatar:
            for avatar in self.model.fetchByField(avatar.id_user, self.model.fields[4], self.db):
                if newName == avatar.name:
                    return None
            avatar.name = newName
            return self._prepareAvatar(self.model.updateOne(avatar, self.db))
        return None

    def fetchAvatarsByName(self, name:str):
        return self._prepareAvatars(self.model.fetchByField(name, self.model.fields[1], self.db))

    def fetchAvatarsByServerElement(self, server:ServerElement):
        avts = []
        for serv in Server.extractServersRedirectTo(server if server.active is True else server.redirectTo):
            for avt in self.fetchAvatarsByServer(Server.indexOfServerElement(serv)):
                avts.append(avt)
        for avt in self.fetchAvatarsByServer(Server.indexOfServerElement(server)):
            avts.append(avt)
        return avts

    def fetchAvatarsByServer(self, server:int):
        return self._prepareAvatars(self.model.fetchByField(server, self.model.fields[2], self.db))

    def fetchAvatarsByServerElementAndJob(self, server:ServerElement, job:int, lvl:int = None):
        out = []
        for avatar in self.fetchAvatarsByServerElement(server):
            if self.fetchJobLvlByAvatarAndJob(avatar.id, job, lvl):
                out.append(avatar)
        return out

    def fetchAvatarsByServerAndJob(self, server:int, job:int, lvl:int = None):
        out = []
        for avatar in self.fetchAvatarsByServer(server):
            if self.fetchJobLvlByAvatarAndJob(avatar.id, job, lvl):
                out.append(avatar)
        return out
    
    def fetchAvatarsByJob(self, job:int):
        out = []
        for avatar in self.fetchAll():
            if self.fetchJobLvlByAvatarAndJob(avatar.id, job):
                out.append(avatar)
        return out

    def fetchAvatarsByJobAndLvl(self, job:int, lvl:int):
        out = []
        for avatar in self.fetchAll():
            self.fetchJobLvlByAvatarAndJob(avatar.id, job, lvl)
        return out

    def fetchAvatarsByDiscordIdAndName(self, user:int, name:str):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            return self._prepareAvatar(self.model.fetchOneByFields([data.id, name], [self.model.fields[4], self.model.fields[1]], self.db))
        return None

    def removeAvatarById(self, avatarId:int):
        avatar = self.fetchAvatarsById(avatarId)
        if avatar:
            for pattern in self.fetchPatternsByAvatarById(avatarId):
                self.removePatternById(avatarId, pattern.id_pattern)
            return self.model.DelByField(avatarId, self.model.fields[0], self.db)
        return None

    def removeAvatar(self, user:int, name:str):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            for pattern in self.fetchPatternsByAvatar(user, name):
                self.removePattern(user, name, pattern.id_pattern)
            return self.model.DelByFields([data.id, name], [self.model.fields[4], self.model.fields[1]], self.db)
        return None

    def setLvl(self, user:int, name:str, lvl:int):
        data = self.fetchAvatarsByDiscordIdAndName(user, name)
        if data:
            data.lvl = lvl
            return self.model.updateOne(data, self.db)
        return None
    
    def fetchJobLvlByAvatarAndJob(self, avatar:int, job:int, lvl:int = None):
        avatarModel = self.model_job.fetchOneByFields([avatar, job], [self.model_job.fields[2], self.model_job.fields[1]], self.db)
        if lvl is not None:
            if avatarModel is not None and avatarModel.lvl >= lvl:
                return avatarModel
        else:
            return avatarModel

    def fetchJobLvlByNameAndJob(self, user:int, name:str, job:int):
        if self.users.fetchUserByDiscordId(user):
            return self.model_job.fetchOneByFields([self.fetchAvatarsByDiscordIdAndName(user, name).id, job], [self.model_job.fields[2], self.model_job.fields[1]], self.db)
        return None

    def setJob(self, user:int, name:str, job:int, lvl:int):
        data = self.fetchJobLvlByNameAndJob(user, name, job)
        if data:
            return self.updateJob(user, name, job, lvl)
        return self.addJob(user, name, job, lvl)

    def updateJob(self, user:int, name:str, job:int, lvl:int):
        avatar = self.fetchAvatarsByDiscordIdAndName(user, name).id
        id = self.fetchJobLvlByAvatarAndJob(avatar, job).id
        data = ModelData(self.model_job.fields, [id, job, avatar, lvl])
        return self.model_job.updateOne(data, self.db)

    def addJob(self, user:int, name:str, job:int, lvl:int):
        data = ModelData(self.model_job.fields, [None, job, self.fetchAvatarsByDiscordIdAndName(user, name).id, lvl])
        return self.model_job.insertOne(data, self.db)

    def fetchPatternsByAvatarById(self, avatarId:int):
        avatar = self.fetchAvatarsById(avatarId)
        if avatar:
            return self.model_pattern.fetchByField(avatar.id, self.model_pattern.fields[1], self.db)
        return None

    def fetchPatternsByAvatar(self, user:int, name:str):
        if self.users.fetchUserByDiscordId(user):
            return self.model_pattern.fetchByField(self.fetchAvatarsByDiscordIdAndName(user, name).id, self.model_pattern.fields[1], self.db)
        return None

    def fetchPatternsByAvatarAndPattern(self, user:int, name:str, pattern:int):
        if self.users.fetchUserByDiscordId(user):
            return self.fetchPatternsByIdAndPattern(self.fetchAvatarsByDiscordIdAndName(user, name).id, pattern)
        return None

    def fetchPatternByIdAndPattern(self, avatar:int, pattern:int):
        return self.model_pattern.fetchOneByFields([avatar, pattern], [self.model_pattern.fields[1], self.model_pattern.fields[2]], self.db)

    def fetchPatternsByIdAndPattern(self, avatar:int, pattern:int):
        return self.model_pattern.fetchByFields([avatar, pattern], [self.model_pattern.fields[1], self.model_pattern.fields[2]], self.db)

    def addPattern(self, user:int, name:str, pattern:int):
        if self.users.fetchUserByDiscordId(user):
            if not self.fetchPatternsByAvatarAndPattern(user, name, pattern):
                data = ModelData(self.model_pattern.fields, [None, self.fetchAvatarsByDiscordIdAndName(user, name).id, pattern])
                return self.model_pattern.insertOne(data, self.db)
        return None

    def removePatternById(self, avatarId:int, pattern:int):
        avatar = self.fetchAvatarsById(avatarId)
        if avatar:
            return self.model_pattern.DelByFields([avatar.id, pattern], [self.model_pattern.fields[1], self.model_pattern.fields[2]], self.db)
        return None

    def removePattern(self, user:int, name:str, pattern:int):
        if self.users.fetchUserByDiscordId(user):
            return self.model_pattern.DelByFields([self.fetchAvatarsByDiscordIdAndName(user, name).id, pattern], [self.model_pattern.fields[1], self.model_pattern.fields[2]], self.db)
        return None

    def fetchFavsofAvatar(self, user:int):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            return self.model_fav.fetchByField(data.id, self.model_fav.fields[1], self.db)

    def fetchFavofAvatarWithId(self, user:int, other:int):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            return self.model_fav.fetchOneByFields([data.id, other], [self.model_fav.fields[1], self.model_fav.fields[2]], self.db)

    def addFav(self, user:int, other:int):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            if not self.fetchFavofAvatarWithId(user, other):
                data = ModelData(self.model_fav.fields, [None, data.id, other])
                return self.model_fav.insertOne(data, self.db)
        return None

    def removeFav(self, user:int, other:int):
        data = self.users.fetchUserByDiscordId(user)
        if data:
            return self.model_fav.DelByFields([data.id, other], [self.model_fav.fields[1], self.model_fav.fields[2]], self.db)
    
    def _prepareAvatar(self, avatar:ModelData, latest:dict = latest):
        if avatar is not None and avatar.id is not None:
            avatar.jobs = []
            cats = []
            for cat in latest['recipeCategories']:
                cats.append(RecipeCategorie(cat))
            for cat in cats:
                job = self.fetchJobLvlByAvatarAndJob(avatar.id ,cat.id)
                lvl = job.lvl if job else None
                avatar.jobs.append({"RecipeCategorie":cat, "Level":lvl})
            avatar.patterns = []
            patterns = []
            for pattern in latest['recipePattern']:
                patterns.append(RecipePattern(pattern))
            for pattern in patterns:
                pat = self.fetchPatternsByIdAndPattern(avatar.id, pattern.id)
                if pat:
                    avatar.patterns.append(RecipePattern.findRecipePattern(latest['recipePattern'], pat[0].id_pattern))
        return avatar

    def _prepareAvatars(self, avatars:list, latest:dict = latest):
        for avatar in avatars:
            if isinstance(avatar, ModelData):
                avatar = self._prepareAvatar(avatar, latest)
        return avatars

    def recipeIsCraftableByAvatar(self, recipe:Recipe, avatarId:int):
        recipe.fetchData(latest, 2)
        job = self.fetchJobLvlByAvatarAndJob(avatarId, recipe.recipeCategorie.id)
        if job:
            if job.lvl >= recipe.level:
                if recipe.recipePattern:
                    if self.fetchPatternByIdAndPattern(avatarId, recipe.recipePattern.id):
                        return True
                else:
                    return True
        return False

    def extractAvatarCanCraftRecipe(self, recipe:Recipe, server:ServerElement):
        srvs = []
        srvs.append(Server.indexOfServerElement(server))
        for serv in Server.extractServersRedirectTo(server):
            srvs.append(Server.indexOfServerElement(serv))
        allAvatars = self.fetchAll()
        fAvatars = []
        for avt in allAvatars:
            if self.recipeIsCraftableByAvatar(recipe, avt.id):
                if not server or (server and avt.server in srvs):
                    fAvatars.append(avt)
        return fAvatars