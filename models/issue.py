from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
from models import User, NoteIssue
import sqlite3, cdnwakfu

class Issue:

    def __init__(self, db:sqlite3.Connection, users:User, with_notes:bool = True):
        fields = [
            Id,
            Integer("owner", True),
            Integer("iid", True),
            Text("repo", True)
        ]
        self.db = db
        self.users = users
        foreign_key = [ForeignKey(fields[1], FieldRef(Id, self.users.model.name))]
        self.model = Model("issues", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key,db=db)
        if with_notes is True:
            self.notes = NoteIssue(self.db , self.users)

    def addOwnerOfIssue(self, user:int, iid:int, repo:str):
        usr = self.users.fetchUserByDiscordId(user)
        if usr:
            id = usr.id
            return self.model.insertOne(ModelData(self.model.fields, [None, id, iid, repo]), self.db)
        return None

    def fetchOwnerOf(self, iid:int, repo:str):
        return self.model.fetchOneByFields([iid, repo], [self.model.fields[2], self.model.fields[3]], self.db)

    def fetchIssueOfUser(self, user:int):
        usr = self.users.fetchUserByDiscordId(user)
        if usr:
            id = usr.id
            return self.model.fetchByField(id, self.model.fields[1], self.db)
        return None

    def fetchIssueOfRepo(self, repo:str):
        return self.model.fetchByField(repo, self.model.fields[3], self.db)

    def fetchAll(self):
        return self.model.fetchAll(self.db)

    def removeIssue(self, id:int):
        return self.model.DelByField(id, self.model.fields[0], self.db)