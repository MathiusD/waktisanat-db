from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
from models import User
from cdnwakfu import BaseItem, Server
import sqlite3, cdnwakfu, time

class Price:

    def __init__(self, item:BaseItem, average:int, minPrice:int = None, maxPrice:int = None):
        self.item = item._id
        self.average = average
        self.min = minPrice
        self.max = maxPrice
        self.timestamp = time.time()

class ItemPrice:

    def __init__(self, db:sqlite3.Connection, users:User):
        fields = [
            Id,
            Integer("owner", True),
            Integer("server", True),
            Real("timestamp", True),
            Integer("item", True),
            Integer("average", True),
            Integer("min"),
            Integer("max")
        ]
        self.db = db
        self.users = users
        foreign_key = [ForeignKey(fields[1], FieldRef(Id, self.users.model.name))]
        self.model = Model("item_price", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key,db=db)

    def addPriceOfItem(self, user:int, server:int, price:Price):
        usr = self.users.fetchUserByDiscordId(user)
        if usr:
            id = usr.id
            return self.model.insertOne(
                ModelData(
                    self.model.fields,
                    [
                        None,
                        id,
                        server,
                        price.timestamp,
                        price.item,
                        price.average,
                        price.min,
                        price.max
                    ]
                ),
            self.db)
        return None

    def fetchLastPriceForAllServer(self, item:BaseItem):
        out = []
        for serv in Server.servers:
            if serv.active == True:
                out.append({
                    "server":serv,
                    "price":self.fetchLastPrice(item, Server.indexOfServerElement(serv))
                })
        return out

    def fetchLastPrice(self, item:BaseItem, serverId:int):
        return self.fetchLastPriceById(item._id, serverId)

    def fetchLastPriceById(self, item:int, serverId:int):
        prices = []
        server = Server.extractServer(serverId)
        if server.active == False:
            for serv in Server.extractServersRedirectTo(server):
                temp = self.model.fetchByFields(
                    [item, Server.indexOfServerElement(serv)],
                    [self.model.fields[4], self.model.fields[2]],
                    self.db
                )
                for pTemp in temp:
                    prices.append(pTemp)
        else:
            prices = self.model.fetchByFields(
                    [item, serverId],
                    [self.model.fields[4], self.model.fields[2]],
                    self.db
                )
        high = None
        for price in prices:
            if high is None:
                high = price
            else:
                if high.timestamp < price.timestamp:
                    high = price
        return high

    def fetchAll(self):
        return self.model.fetchAll(self.db)