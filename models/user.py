from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
import sqlite3

class User:

    def __init__(self, db:sqlite3.Connection=None):
        fields = [
            Id,
            TextWithoutCaseSensitive("name", True),
            Integer("discord_id", True),
            Text("lang", True),
            Integer("notify", True),
            Text("prefix")
        ]
        self.db = db
        self.model = Model("User", fields=fields, primary_key=Default_PrimaryKey, db=db)

    def addUser(self, name:str, discord_id:int, lang:str, notify:bool, prefix:str = None):
        if len(self.model.fetchByField(discord_id, self.model.fields[2], self.db)) == 0:
            return self.model.insertOne(ModelData(self.model.fields, [None, name, discord_id, lang, notify, prefix]), self.db)
        return None

    def updateUserPrefix(self, discord_id:int, prefix:str, verbose:bool = False):
        data = self.model.fetchByField(discord_id, self.model.fields[2], self.db)
        if len(data) > 0:
            for dat in data:
                dat.prefix = prefix
            return self.model.updateMany(data, self.db, verbose=verbose)
        return None

    def updateUserLang(self, discord_id:int, lang:str, verbose:bool = False):
        data = self.model.fetchByField(discord_id, self.model.fields[2], self.db)
        if len(data) > 0:
            for dat in data:
                dat.lang = lang
            return self.model.updateMany(data, self.db, verbose=verbose)
        return None

    def updateUserName(self, discord_id:int, name:str, verbose:bool = False):
        data = self.model.fetchByField(discord_id, self.model.fields[2], self.db)
        if len(data) > 0:
            for dat in data:
                dat.name = name
            return self.model.updateMany(data, self.db, verbose=verbose)
        return None

    def updateUserNotify(self, discord_id:int, notify:bool, verbose:bool = False):
        data = self.model.fetchByField(discord_id, self.model.fields[2], self.db)
        if len(data) > 0:
            for dat in data:
                dat.notify = notify
            return self.model.updateMany(data, self.db, verbose=verbose)
        return None

    def fetchUserByName(self, name:str):
        return self.model.fetchOneByField(name, self.model.fields[1], self.db)
        
    def fetchUserById(self, id:int):
        return self.model.fetchOneByField(id, self.model.fields[0], self.db)
    
    def fetchUserByDiscordId(self, user:int):
        return self.model.fetchOneByField(user, self.model.fields[2], self.db)

    def fetchLangOfUser(self, user:int):
        request = self.model.fetchOneByField(user, self.model.fields[2], self.db)
        return request.lang if request else None

    def fetchPrefixOfUser(self, user:int):
        request = self.model.fetchOneByField(user, self.model.fields[2], self.db)
        return request.prefix if request else None

    def fetchAll(self):
        return self.model.fetchAll(self.db)

    def removeUser(self, user:int):
        data = self.fetchUserByDiscordId(user)
        if data:
            return self.model.DelByField(data.id, self.model.fields[0], self.db)
        return None