from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
from models import Avatar
import sqlite3, cdnwakfu, time

class ItemRequest:

    def __init__(self, db:sqlite3.Connection, avatars:Avatar, nameModel:str = "ItemRequest"):
        fields = [
            Id,
            Integer("id_avatar", True),
            Integer("id_item", True),
            Integer("quantity", True),
            Real("created", True),
            Real("last_edtition", True)
        ]
        self.db = db
        self.avatars = avatars
        foreign_key = [ForeignKey(fields[1], FieldRef(Id, self.avatars.model.name))]
        self.model = Model(nameModel, fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key,db=db)

    def addRequest(self, user:int, name:str, item:int, quantity:int = 1):
        avt = self.avatars.fetchAvatarsByDiscordIdAndName(user, name)
        if avt:
            id = avt.id
            data = self.model.fetchByFields([item, id], [self.model.fields[2], self.model.fields[1]], self.db)
            if len(data) ==  0:
                timeVal = time.time()
                return self.model.insertOne(ModelData(self.model.fields, [None, id, item, quantity, timeVal, timeVal]), self.db)
            elif len(data) == 1:
                dat = data[0]
                dat.quantity += quantity
                dat.last_edtition = time.time()
                return self.model.updateOne(ModelData, self.db)
        return None

    def addRequestWithItem(self, user:int, name:str, item:cdnwakfu.BaseItem, quantity:int = 1):
        if item:
            return self.addRequest(user, name, item._id, quantity)
        return None

    def fetchAll(self):
        return self.model.fetchAll(self.db)

    def fetchRequestByAvatar(self, user:int, name:str):
        avt = self.avatars.fetchAvatarsByDiscordIdAndName(user, name)
        if avt:
            id = avt.id
            return self.model.fetchByField(id, self.model.fields[1], self.db)
        return None

    def fetchRequestByItem(self, item:int):
        return self.model.fetchByField(item, self.model.fields[2], self.db)

    def addRequestByItemWithItem(self, item:cdnwakfu.BaseItem):
        if item:
            return self.fetchRequestByItem(item._id)
        return None

    def fetchAvatarsByAvatarAndItem(self, user:int, name:str, item:int):
        avt = self.avatars.fetchAvatarsByDiscordIdAndName(user, name)
        if avt:
            id = avt.id
            return self.model.fetchOneByFields([id, item], [self.model.fields[1], self.model.fields[2]], self.db)
        return None

    def fetchAvatarsByAvatarAndItemWithItem(self, user:int, name:str, item:cdnwakfu.BaseItem):
        if item:
            return self.fetchAvatarsByAvatarAndItem(user, name, item._id)
        return None

    def fetchRequestById(self, id:int):
        return self.model.fetchOneByField(id, self.model.fields[0], self.db)

    def fetchAfterId(self, id:int):
        return self.model.fetchByField(id, self.model.fields[0], self.db, ">")

    def updateRequest(self, id:int, quantity:int):
        data = self.fetchRequestById(id)
        if data:
            data.quantity = quantity
            data.last_edtition = time.time()
            return self.model.updateOne(data, self.db)
        return None

    def removeRequest(self, user:int, name:str, item:int):
        temp = self.fetchAvatarsByAvatarAndItem(user, name, item)
        if temp:
            id = temp.id
            return self.model.DelByField(id, self.model.fields[0], self.db)
        return None

    def removeRequestWithItem(self, user:int, name:str, item:cdnwakfu.BaseItem):
        if item:
            return self.removeRequest(user, name, item._id)
        return None

    def removeAllRequestById(self, avatarId:int):
        avt = self.avatars.fetchAvatarsById(avatarId)
        if avt:
            return self.model.DelByField(avt.id, self.model.fields[1], self.db)
        return None

    def removeAllRequest(self, user:int, name:str):
        avt = self.avatars.fetchAvatarsByDiscordIdAndName(user, name)
        if avt:
            id = avt.id
            return self.model.DelByField(id, self.model.fields[1], self.db)
        return None