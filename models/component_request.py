from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
from models import Avatar
import sqlite3
from models import ItemRequest

class ComponentRequest(ItemRequest):

    def __init__(self, db:sqlite3.Connection, avatars:Avatar):
        super().__init__(db, avatars, "ComponentRequest")