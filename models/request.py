from models import CraftRequest, ComponentRequest, BuyRequest, Avatar
import sqlite3

class Request:

    def __init__(self, db:sqlite3.Connection, avatars:Avatar):
        self.craftRequests = CraftRequest(db, avatars)
        self.componentRequests = ComponentRequest(db, avatars)
        self.buyRequests = BuyRequest(db, avatars)
        self.avatars = avatars

    def removeRequestForAvatar(self, avatarId:int):
        self.craftRequests.removeAllRequestById(avatarId)
        self.componentRequests.removeAllRequestById(avatarId)
        self.buyRequests.removeAllRequestById(avatarId)