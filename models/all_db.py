from own.own_sqlite.models import ModelData
import sqlite3, models

class AllDb:


    def __init__(self, db:sqlite3.Connection):
        self.db = db
        self.users = models.User(db)
        self.avatars = models.Avatar(db, self.users)
        self.requests = models.Request(db, self.avatars)
        self.prices = models.ItemPrice(db, self.users)
        self.issues = models.Issue(db, self.users)
    
    def removeUserByDiscordID(self, userDiscordId:int):
        user = self.users.fetchUserByDiscordId(userDiscordId)
        if user:
            self.removeUser(user)

    def removeUserById(self, userId:int):
        user = self.users.fetchUserById(userId)
        if user:
            self.removeUser(user)

    def removeUser(self, user:ModelData):
        avatars = self.avatars.fetchAvatarsByDiscordId(user.discord_id)
        for avatar in avatars:
            self.removeAvatar(avatar)
        for issue in self.issues.fetchIssueOfUser(user.discord_id):
            self.issues.removeIssue(issue.id)
        for note in self.issues.notes.fetchNoteOfUser(user.discord_id):
            self.issues.notes.removeNote(note.id)
        self.users.removeUser(user.discord_id)

    def removeAvatarById(self, avatarId:int):
        avatar = self.avatars.fetchAvatarsById(avatarId)
        if avatar:
            self.removeAvatar(avatar)
    
    def removeAvatar(self, avatar:ModelData):
        avatar = self.avatars.removeAvatarById(avatar.id)
        self.requests.removeRequestForAvatar(avatar.id)
        return avatar