from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
from models import User
import sqlite3, cdnwakfu

class NoteIssue:

    def __init__(self, db:sqlite3.Connection, users:User):
        fields = [
            Id,
            Integer("owner", True),
            Integer("iid", True),
            Text("repo", True),
            Integer("note_id", True)
        ]
        self.db = db
        self.users = users
        foreign_key = [ForeignKey(fields[1], FieldRef(Id, self.users.model.name))]
        self.model = Model("notes_issues", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key,db=db)

    def addOwnerOfNote(self, user:int, iid:int, repo:str, note_id:int):
        usr = self.users.fetchUserByDiscordId(user)
        if usr:
            id = usr.id
            return self.model.insertOne(ModelData(self.model.fields, [None, id, iid, repo, note_id]), self.db)
        return None

    def fetchOwnerOf(self, iid:int, repo:str, note_id:int):
        return self.model.fetchOneByFields([iid, repo, note_id], [self.model.fields[2], self.model.fields[3], self.model.fields[4]], self.db)

    def fetchNoteOfUser(self, user:int):
        usr = self.users.fetchUserByDiscordId(user)
        if usr:
            id = usr.id
            return self.model.fetchByField(id, self.model.fields[1], self.db)
        return None

    def fetchNoteOfIssue(self, repo:str, iid:int):
        return self.model.fetchByFields([repo, iid], [self.model.fields[3], self.model.fields[2]], self.db)

    def fetchAll(self):
        return self.model.fetchAll(self.db)

    def removeNote(self, id:int):
        return self.model.DelByField(id, self.model.fields[0], self.db)